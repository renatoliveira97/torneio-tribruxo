import { useEffect } from 'react';
import './style.css';

import StudentCard from '../StudentCard';

const Student = ({ HandlePlay, students, setStudents }) => {

    useEffect(() => {
        fetch("https://hp-api.herokuapp.com/api/characters/students")
        .then((response) => response.json())
        .then((response) => setStudents(response))
        .catch((error) => console.log(error));
    }, [setStudents]);

    return (
        <>
            <div className="studentCards">
                <StudentCard students={students}/>
                <StudentCard students={students}/>
                <StudentCard students={students}/>           
            </div>
            <button className="retryButton" onClick={HandlePlay}>Tentar novamente</button> 
        </>
    );
}

export default Student;