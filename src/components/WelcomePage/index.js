import "./style.css";

const WelcomePage = ({ HandlePlay }) => {
    return (
        <>
            <h1> Torneio tribruxo</h1>
            <p>Clique no botão para<br/>encontrar os feiticeiros!</p>
            <button onClick={HandlePlay}>Começar!</button>
        </>
    );
}

export default WelcomePage;