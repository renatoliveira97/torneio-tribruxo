const StudentCard = ({ students }) => {

    let person = students[Math.floor(Math.random() * students.length)];

    return (
        <div>
            {person !== undefined ? (
                <div className="person">
                    <div className="imageFrame">
                        <img src={person.image} alt={person.name}/>
                    </div>
                    <p className="personDescription">{person.name}</p>
                    <p className="personDescription">{person.house}</p>
                </div>
                ) :
                console.log("objeto não carregado!") 
            }   
        </div>
        
    )
}

export default StudentCard;