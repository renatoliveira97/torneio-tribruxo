import './App.css';
import { useState } from 'react';

import WelcomePage from "./components/WelcomePage";
import Student from "./components/Student";

const App = () => {

  const [play, setPlay] = useState(false);
  const [students, setStudents] = useState([]);

  const HandlePlay = () => {
    setPlay(!play);   
  }

  return (
    <div className="App">
      <header className="App-header">
        {play ? (
        <Student HandlePlay={HandlePlay} students={students} setStudents={setStudents}/>
        ) : (
        <WelcomePage HandlePlay={HandlePlay}/>
        )}
      </header>
    </div>
  );
}

export default App;
